from flask import Flask, redirect, url_for, session, request, render_template
import sql_queries
import os
from random import shuffle


def start_quiz():
    session['quiz'] = request.form['quiz']
    session['question'] = 0
    session['total'] = 0
    session['correct'] = 0
    session['score'] = 0
    session['total_score'] = 0
    session['complete'] = False
    session['answered'] = True


def index():
    return render_template('index.html')


def manage():
    return render_template('manage.html')


def add_question():
    if request.method == 'POST':
        sql_queries.add_question(request.form['question'], request.form['correct'], request.form['wrong1'],
                                 request.form['wrong2'], request.form['wrong3'], request.form['score'])
    return render_template('add_question.html')


def add_quiz():
    if request.method == 'POST':
        quiz_name = request.form['quiz_name']
        questions = request.form['hidden_selected_questions'].split(',')
        quiz_id = sql_queries.add_quiz(quiz_name)
        for question in questions:
            sql_queries.add_quiz_content(question, quiz_id)
    questions = sql_queries.get_questions()
    return render_template('add_quiz.html', questions=questions)


def quiz():
    if request.method == 'POST':
        start_quiz()
        return redirect(url_for('test'))
    elif request.method == 'GET':
        quizzes = sql_queries.get_quizzes()
        return render_template('quiz.html', q_list=quizzes)


def test():
    if 'quiz' not in session:
        return redirect(url_for('index'))
    if request.method == 'POST':
        answer = request.form['answer']
        result = sql_queries.check_answer(session['question'], answer)
        if result[0]:
            session['correct'] += 1
            session['score'] += result[1]
        session['total'] += 1
        session['total_score'] += result[1]
        session['answered'] = True
    result = None
    if session['answered'] is True:
        result = sql_queries.get_question_after(session['question'], session['quiz'])
        session['answered'] = False
    else:
        result = sql_queries.get_question(session['question'], session['quiz'])
    if result is None or len(result) == 0:
        session['complete'] = True
        return redirect(url_for('result'))
    else:
        session['question'] = result[0]
        answers = [result[2], result[3], result[4], result[5]]
        shuffle(answers)
        return render_template('test.html', a_list=answers, question=result[1])


def result():
    if 'complete' not in session or session['complete'] is False:
        return redirect(url_for('index'))
    correct = session['correct']
    total = session['total']
    score = session['score']
    total_score = session['total_score']
    return render_template('result.html', correct=correct, total=total, score=score, total_score=total_score)


folder = os.getcwd()
app = Flask(__name__, static_folder=folder, template_folder=folder)
app.config['SECRET_KEY'] = 'alskd;kljadsnfkjadsfkj;ndskj;fs'
app.add_url_rule('/', 'index', index)
app.add_url_rule('/manage', 'manage', manage)
app.add_url_rule('/add_question', 'add_question', add_question, methods=['GET', 'POST'])
app.add_url_rule('/add_quiz', 'add_quiz', add_quiz, methods=['GET', 'POST'])
app.add_url_rule('/quiz', 'quiz', quiz, methods=['GET', 'POST'])
app.add_url_rule('/test', 'test', test, methods=['GET', 'POST'])
app.add_url_rule('/result', 'result', result)

if __name__ == '__main__':
    app.run(host='192.168.0.10', port=80)
