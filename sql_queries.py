import sqlite3

db_name = 'quiz.sqlite'
conn = None
cursor = None


def print_table(table):
    for entry in table:
        print(entry)


def open():
    global conn, cursor
    conn = sqlite3.connect(db_name)
    cursor = conn.cursor()


def close():
    cursor.close()
    conn.close()


def do(query):
    cursor.execute(query)
    conn.commit()


def create():
    open()
    cursor.execute('''PRAGMA foreign_keys=on''')

    do('''CREATE TABLE IF NOT EXISTS quiz (
           id INTEGER PRIMARY KEY,
           name VARCHAR)''')

    do('''CREATE TABLE IF NOT EXISTS question (
               id INTEGER PRIMARY KEY,
               question VARCHAR,
               answer VARCHAR,
               wrong1 VARCHAR,
               wrong2 VARCHAR,
               wrong3 VARCHAR,
               score INTEGER)''')

    do('''CREATE TABLE IF NOT EXISTS quiz_content (
               id INTEGER PRIMARY KEY,
               quiz_id INTEGER,
               question_id INTEGER,
               FOREIGN KEY (quiz_id) REFERENCES quiz (id),
               FOREIGN KEY (question_id) REFERENCES question (id) )''')
    close()


def add_quizzes():
    quizzes = [
        (0, 'Кто хочет стать миллионером?'),
        (1, 'Своя игра'),
        (2, 'Самый умный')
    ]
    open()
    cursor.executemany('insert into quiz (id, name) values (?, ?)', quizzes)
    conn.commit()
    close()


def add_questions():
    questions = [
        (0, 'Сколько месяцев в году имеют 28 дней?', 'Все', 'Один', 'Ни одного', 'Два', 1),
        (1, 'Каким станет зелёный утёс, если упадёт в Красное море?', 'Мокрым', 'Красным', 'Не изменится', 'Фиолетовым', 2),
        (2, 'Какой рукой лучше размешивать чай?', 'Ложкой', 'Правой', 'Левой', 'Любой', 1),
        (3, 'Что не имеет длины, глубины, ширины, высоты, а можно измерить?', 'Время', 'Глупость', 'Море', 'Воздух', 3),
        (4, 'Когда сетью можно вытянуть воду?', 'Когда вода замерзла', 'Когда нет рыбы', 'Когда уплыла золотая рыбка', 'Когда сеть порвалась', 1),
        (5, 'Что больше слона и ничего не весит?', 'Тень слона', 'Воздушный шар', 'Парашют', 'Облако', 2)
    ]
    open()
    cursor.executemany('insert into question (id, question, answer, wrong1, wrong2, wrong3, score) values (?, ?, ?, ?, ?, ?, ?)', questions)
    conn.commit()
    close()


def add_quiz_content_old():
    content = [
        (0, 0),
        (0, 1),
        (0, 3),
        (0, 5),
        (1, 2),
        (1, 1),
        (1, 4),
        (1, 3),
        (2, 0),
        (2, 2),
        (2, 4),
        (2, 5)
    ]
    open()
    cursor.executemany('insert into quiz_content (quiz_id, question_id) values (?, ?)', content)
    conn.commit()
    close()


def clear_db():
    open()
    do('drop table if exists quiz_content')
    do('drop table if exists quiz')
    do('drop table if exists question')
    conn.commit()
    close()


def show(table):
    query = 'select * from ' + table
    open()
    cursor.execute(query)
    print(cursor.fetchall())
    close()


def show_tables():
    show('quiz')
    show('question')
    show('quiz_content')


def join_tables():
    open()
    do('''select name, question, answer from quiz
        join quiz_content qc on quiz.id = qc.quiz_id
        join question q on qc.question_id = q.id''')
    print_table(cursor.fetchall())
    close()


def quiz_questions(quiz_id):
    open()
    query = '''select name, question, answer, score from quiz
        join quiz_content qc on quiz.id = qc.quiz_id
        join question q on qc.question_id = q.id
        where quiz.id = ?'''
    cursor.execute(query, [quiz_id])
    print_table(cursor.fetchall())
    close()


#Викторины и кол-во баллов, которое можно в них набрать
def quiz_info(quiz_id):
    open()
    query = '''select quiz.id, quiz.name, sum(score) from quiz
            join quiz_content qc on quiz.id = qc.quiz_id
            join question q on qc.question_id = q.id
            where quiz.id = ?
            group by quiz.id'''
    cursor.execute(query, [quiz_id])
    print_table(cursor.fetchall())
    close()


def get_question_after(question_id=0, quiz_id=1):
    open()
    query = '''
    SELECT quiz_content.id, question.question, question.answer, question.wrong1, question.wrong2, question.wrong3
    FROM question JOIN quiz_content on question.id = quiz_content.question_id
    WHERE quiz_content.id > ? AND quiz_content.quiz_id = ?
    ORDER BY quiz_content.id'''
    cursor.execute(query, [question_id, quiz_id])
    result = cursor.fetchone()
    close()
    return result


def get_question(question_id=0, quiz_id=1):
    open()
    query = '''
    SELECT quiz_content.id, question.question, question.answer, question.wrong1, question.wrong2, question.wrong3
    FROM question JOIN quiz_content on question.id = quiz_content.question_id
    WHERE quiz_content.id = ? AND quiz_content.quiz_id = ?'''
    cursor.execute(query, [question_id, quiz_id])
    result = cursor.fetchone()
    close()
    return result


def get_quiz_count():
    open()
    query = '''select count(*) from quiz'''
    cursor.execute(query)
    result = cursor.fetchall()[0][0]
    close()
    return result


def get_quizzes():
    open()
    query = '''select * from quiz order by id'''
    cursor.execute(query)
    result = cursor.fetchall()
    close()
    return result


def get_questions():
    open()
    query = '''select id, question from question order by id'''
    cursor.execute(query)
    result = cursor.fetchall()
    close()
    return result


def check_answer(id, answer):
    open()
    query = '''select q.answer, q.score from quiz_content qc
    join question q on qc.question_id = q.id
    where qc.id = ?'''
    cursor.execute(query, (id,))
    result = cursor.fetchone()
    close()
    correct = result is not None and result[0] == answer
    score = 0
    if result is not None:
        score = result[1]
    return correct, score


def add_question(question, correct, wrong1, wrong2, wrong3, score):
    open()
    query = '''insert into question (question, answer, wrong1, wrong2, wrong3, score) values (?, ?, ?, ?, ?, ?)'''
    cursor.execute(query, [question, correct, wrong1, wrong2, wrong3, score])
    conn.commit()
    close()


def add_quiz(name):
    open()
    query = '''insert into quiz (name) values(?)'''
    cursor.execute(query, [name])
    conn.commit()
    cursor.execute('''select last_insert_rowid()''')
    result = cursor.fetchone()
    close()
    return result[0]


def add_quiz_content(question_id, quiz_id):
    open()
    query = '''insert into quiz_content (question_id, quiz_id) values(?, ?)'''
    cursor.execute(query, [question_id, quiz_id])
    conn.commit()
    close()


def main():
    show_tables()


if __name__ == "__main__":
    main()
